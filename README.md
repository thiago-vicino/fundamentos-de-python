# Fundamentos de python

## Ativar ambiente virtual

```
git pull origin main && conda activate env_python_thiagovicino && conda env update
```

## Versionar código

```
git add .
```

```
git commit -m "mensagem"
```

```
git pull origin main
```

```
git push origin main
```
#### variável
 É uma *etiqueta* colocada em determinado valor. Quando precisamos desse valor, "chamamos" através da etiqueta
- operador de atribuição ```=```
- regras de nomeação de variáveis:
  - Não podem ter espaço e devem começar com letra ou sublinhado
  - Não podem usar palavras reservadas ou identificadores internos que tenham finalidades importantes em Python
  - Por convenção, pede-se que se utilize apenas letras minúsculas e sublinhado para nomear as variáveis

#### f-string
 É uma série formada pela junção de strings com variáveis e expressões
 É uma forma simples que o python dá para mexer com strings com 
>> Para se definir uma f-string, deve-se colocar f seguido de aspas simples, sendo que funções, variáveis ou outras expressões devem ser colocadas entre chaves: ```f'string, {expressao} e/ou {variável_x}'```

## 1. Quais são os tipos de dados básicos?
 Os tipos de dados básicos são três: 

#### string (str)
 É uma série ordenada e imutável de caracteres (números, letras, símbolos, espaços e pontuações) 
>> Para se definir uma string, ela deve ser colocada entre aspas simples ```'string'```


#### número (int, float, complex)

```

```

### booleanos
True
False

## 2. Quais são os tipos de dados avançados?
 Os tipos de dados avançados são: 
- listas (list): tipo de estrutura de dados que é uma coleção ordenada de elementos; valores entre colchetes
    Na lista, é possível modificar, adicionar ou excluir elementos
    É iterável

- tupla (tuple): tipo de estrutura de dados que é uma coleção ordenada de elementos; valores entre parênteses
    Na tupla, não é possível modificar seus elementos
    Serve para passar uma sequência de dados que não pode mudar
    É iterável 
```

```
- conjuntos (set): valores entre chaves


- dicionário: tipo de estrutura de dados que é uma coleção associativa desordenada de elementos (operação de mapeamento), composta por pares de:
chave (key)
valor/es (value)

## 3. O que são e para que servem as funções? Qual a estrutura básica de uma função?
 A função é um bloco de código organizado e reutilizável que realiza uma sequência de instruções
>> Pode ter PARAMETROS e receber ARGUMENTOS
Estrutura básica de uma função:
*************** adicionar o if name e a main *******************
```
def ex_funcao1():
    lista_elemtentos = ['item_a', 'item_b', 'item_c']
    return f'Esta é a lista dos elementos i: {lista_elementos}'

def ex_funcao2(parametro_x, parametro_y = 'argumento_b'):
    parametro_x = 'argumento_a'
    variavel_v = title(parametro_x,parametro_y)
    return variavel_v

ex_funcao1()
```
Funções built-in (nativas do python): 
```
print(), type(), input()
```
>> Para encontrar todas as funções nativas, printar o 'dir' do tipo de dado básico, por exemplo: 
```
print(dir(variável_x))
print(dir(str))
print(dir(list))
```
## 4. O que são e para que servem as classes? Qual a estrutura básica de uma classe?
 A classe é um *molde*, que funciona com um conjunto de funções e serve para organizá-las pelos parâmetros que lhes são afins, por exemplo. As funções dentro de uma classe são chamadas de métodos. 
 >> Atenção para a identação (alinhamento de cada linha no código).
 Estrutura básica de uma classe:
```
class Exemplo:
    def __init__(self, parametro_x, parametro_y, parametro_z='atributo_a'): 
        self.parametro_x = parametro_x #estes são os atributos que serão passados
        self.parametro_y = parametro_y
        self.parametro_z = parametro_z
        #'atributo_a' pode ser sobrescrito se for passado um outro atributo para o parametro_z

    def funcao1(self):
        # [...]
        pass

    def funcao2(self, parametro_w, parametro_k):
        return parametro_w + parametro_k
```

## 5. O que são e para que servem as funções lambda,map(),reduce(),filter()? Quais as estruturas básicas dessas funções?
 Estas funções são uma forma simplificada de fazer operações com itens de uma lista. As expressões que usamos dentro dessas funções podem ser as nativas do python ou outras criadas por nós mesmos. Também podemos usar a função lambda como expressão() das demais, por exemplo. A seguir, tem-se o funcionamento e a estrutura de cada função:
Dada a lista: 
```
lista_itens = ['item x', 'item y', 'item z']
```
### Função lambda 
Faz operações em geral com números. Estrutura básica da função lambda:
```
exemplo1 = lambda p,q:p+q
exemplo2 = lambda p,q: p if(p<q) else q
```
### Função map() 
Realiza a operação de uma expressao() com cada item de uma lista. Estrutura básica da função map:
```
lista_elementos = list(map(expressao(), lista_itens))
```
### Função reduce() 
Retorna um único dado ou item que satisfaz uma expressão():
a) Na linha 1, importar função reduce:
```
from functools import reduce
```
b) Estrutura básica da função reduce():
```
reduce(expressão(), lista_itens)
```
### Função filter() 
Filtra os itens de uma lista que satisfazem uma expressão(). Estrutura básica da função filter:
```
lista_elementos = list(filter(expressão(), lista_elementos))
```

## 6. O que é e para que serve a compreenssão de listas? Qual a estrutura básica?
 A compreensão de listas é uma função simplificada usada dentro uma lista vazia para incluir itens de outra lista nela, segundo as regras que satisfazem a expressão(). Estrutura básica da compreensão de listas:
```
lista_elementos = [expressão() for item in lista_itens]
```
