from aula02_funcoes import numeros, somar, subtrair, multiplicar, dividir

def inverter(num_a, num_b, num_c):
    inverter = -(num_a + num_b + num_c)
    print(inverter)
    return inverter

def main ():
    num_a , num_b , num_c = numeros()
    somar_numeros = somar(num_a , num_b , num_c)
    subtrair_numeros = subtrair(num_a , num_b , num_c)
    dividir_numeros = dividir(num_a , num_b , num_c)
    multiplicar_numeros = multiplicar(num_a , num_b , num_c)
    inversão = inverter(num_a , num_b , num_c)

if __name__ == "__main__": 
    main ()