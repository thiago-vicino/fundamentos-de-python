from functools import reduce

class Cardapio_geral:
    def cardapio_pizzas(self):
        sabores = [('Atum', 40.0, 60.0, 6.0), ('Bacon', 64.0, 84.0, 8.0), ('Portuguesa', 64.0, 72.0, 8.0), ('Mexicana', 56, 60.0, 7.0)]
        formatos = ['Redonda (8 pedaços)', 'Quadrada (12 pedaços)', 'Uma fatia', 'Fatias avulsas. Valor por fatia:']
        brindes = ['imã', 'chaveiro', 'calendário', 'abridor de latas']
        print(f'\n#Cardápio: \n##Pizzas:')
        for index,(sabor, brinde, formato) in enumerate(zip(sabores, brindes, formatos), start=1):
            print(f'{index} - {sabor[0]} - {formatos[0]} - R$ {sabor[1]} ⎪ {formatos[1]} - R$ {sabor[2]} ⎪ {formatos[2]} - R$ {sabor[3]} ⎪ {formatos[3]} - R$ {sabor[3]} ⎪ Brinde: {brinde}')
        menor_preco = input('------\nGostaria de saber a pizza com menor valor? [digite sim ou não] ').lower()
        if menor_preco == 'sim':
            valores_pizzas_redondas = [int(sabores[0][1]), int(sabores[1][1]), int(sabores[2][1]), int(sabores[3][1])]
            preco_redondas = reduce((lambda x,y: x if(x<y) else y), valores_pizzas_redondas)
            print(f'\nA pizza mais barata do dia é a de sabor {sabores[valores_pizzas_redondas.index(preco_redondas)][0]}, no valor de R$ {preco_redondas}')
        pizzas = [sabores[0][0], sabores[1][0], sabores[2][0], sabores[3][0]]
        cardapio_so_sabores = [item for item in pizzas]
        return f'\nDentre os sabores a seguir: \n {cardapio_so_sabores} \n'

    def cardapio_bebidas(self):
        bebidas = [('Chá verde (500mL):', 5.70), ('Guaraná (2L):', 7.20), ('vinho tinto (900mL):', 25.00), ('suco de laranja (1,5L):', 6.90)]
        print('\n##Bebidas: \n')
        for index,bebida in enumerate(bebidas, start = 1):
            print(f'{index} - {bebida[0]} - R$ {bebida[1]}') #PASSAR O DESCONTO AQUI TAMBÉM
        print('\nDesconto de 50% no valor das bebidas: \n')
        valor_bebida = [item[1] for item in bebidas]
        lista_desconto = list(map(lambda x: x/2, valor_bebida))
        print(f'Os valores com desconto são respectivamente:')
        for index, (bebida, desconto)  in enumerate(zip(bebidas, lista_desconto), start=1):
            print(f'{index} - {bebida[0]} R$ {desconto}')
        return ''

class Pizza:
    def __init__(self, sabor, formato, bebida): #padrão (pode ser substituído se for passado novo formato)
        self.sabor = sabor
        self.formato = formato
        self.bebida = bebida
 
    def ingredientes(self):
        if self.sabor == 'atum':
            return f'Ingredientes da pizza sabor {self.sabor}: muçarela, azeitona, tomate, atum, cebola e orégano.' 
        elif self.sabor == 'bacon':
            return f'Ingredientes da pizza sabor {self.sabor}: muçarela coberta com bacon, molho de tomate, azeitona e orégano.'
        elif self.sabor == 'portuguesa':
            return f'Ingredientes da pizza sabor {self.sabor}: molho de tomate, muçarela, presunto, palmito, ervilha, lombo canadense fatiado, provolone ralado e ovo.'
        else:
            return f'Ingredientes da pizza sabor {self.sabor}: molho de tomate, muçarela, calabresa fatiada, pimentão fatiado e pimenta dedo de moça.' 

    def tempo_de_espera (self):
        return 'Tempo médio de espera: 40 minutos'
        
    def valor_bebidas(self): 
        if self.bebida == 1: 
            return 'chá verde (500mL)', 5.70/2
        elif self.bebida == 2:
            return 'guaraná (2L)', 7.20/2
        elif self.bebida == 3:
            return 'vinho tinto (900mL)', 25.00/2
        else: 
            return 'suco de laranja (1,5L)', 6.90/2

class Atum (Pizza):
    def __init__(self, formato, bebida, sabor='atum'):
        super().__init__(formato, bebida, sabor)
        self.cupom = 0.85
        self.brinde = 'imã'

    def tempo_de_espera (self):
        return 'Tempo médio de espera: 30 minutos'   

    def preco (self):
        if self.formato == 12:
            return 60*self.cupom
        else: 
            return self.formato*6

class Bacon (Pizza):
    def __init__(self, formato, bebida, sabor='bacon'):
        super().__init__(formato, bebida, sabor)
        self.brinde = 'chaveiro'

    def preco (self):
        if self.formato == 12:
            return 84
        else:
            return self.formato*8

class Portuguesa (Pizza): 
    def __init__(self, formato, bebida, sabor='portuguesa'):
        super().__init__(formato, bebida, sabor)
        self.brinde = 'calendário'

    def tempo_de_espera (self):
        return 'Tempo médio de espera: 50 minutos'

    def preco(self):
        if self.formato == 12:
            return 72
        else:
            return self.formato*8

class Mexicana (Pizza):
    def __init__(self, formato, bebida, sabor='mexicana'):
        super().__init__(formato, bebida, sabor)
        self.brinde = 'abridor de latas'

    def preco (self):
        if self.formato == 12:
            return 60
        else:
            return self.formato*7
    
def main():
    while True:
        print(Cardapio_geral().cardapio_pizzas())
        sabor = input('\nPor favor, escolha o sabor de sua pizza: ').lower()
        while True: # enquanto não digitar um valor válido, pede que digite novamente
            try:
                formato = int(input('\nPor favor, insira o número de pedaços da pizza: '))
                if 1 <= formato <= 12:
                    break # valor válido, sai do while
                print('> O valor deve estar entre 1 e 12.') # se não entrou no if acima, o valor é inválido
            except ValueError: # não foi digitado um número
                print('Digite um número válido.')
        print(Cardapio_geral().cardapio_bebidas())
        while True:
            try: 
                bebida = int(input('\nEscolha o número da sua bebida: '))
                if 1 <= bebida <= 4:
                    break 
                print('> O valor deve estar entre 1 e 4.')
            except ValueError: 
                print('Digite um número válido.')
        if sabor == 'atum':
            pedido = Atum(sabor, formato, bebida)
        elif sabor == 'portuguesa':
            pedido = Portuguesa(sabor, formato, bebida)
        elif sabor == 'bacon':
            pedido = Bacon(sabor, formato, bebida)
        else:
            pedido = Mexicana(sabor, formato, bebida)
        print(f'\nO seu pedido final foi:\nPizza sabor: {sabor.title()}, formato {"quadrada" if formato == 12 else "redonda"} ({formato} pedaço{"" if formato == 1 else "s"}).')
        print(f'{pedido.ingredientes()}\nValor da pizza: R$ {pedido.preco()}.\nBebida sabor {pedido.valor_bebidas()[0]} no valor de {pedido.valor_bebidas()[1]}.\nVocê ganha de brinde um {pedido.brinde}.') #ACRESCENTAR DESCONTO (NOS CASOS EM QUE HOUVE)
        print(f'--------\nO preço final é: R$ {reduce(lambda x,y: x+y, [pedido.preco(), pedido.valor_bebidas()[1]])}\n--------\n{pedido.tempo_de_espera()}\n')

if __name__ == "__main__": 
    main()