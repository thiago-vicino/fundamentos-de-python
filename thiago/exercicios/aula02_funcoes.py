# Funções built-in - nativas
# print(), type(), input()

# Funções feitas pelo utilizador

"""
>> Em linhas gerais, uma função é um bloco de código organizado e reutilizável que realiza uma sequência de instruções
>> Pode ter PARAMETROS e receber ARGUMENTOS


# Funções built-in - nativas
# print(), type(), input()

"""

def nome_da_função():
    print("Olá Mundo!")

nome_da_função()

# TAREFA (21/10): utilizar o input
# TAREFA (21/10): montar função de multiplicação e subtração
def somar(num_a, num_b):
    soma = print(f'A soma de {num_a} + {num_b} é igual a: {num_a + num_b}')
    return soma

somar(3,10)

def subtrair():
    soma = somar(10,20)
    return soma

subtrair()

def multiplicar():
    soma = somar()


def nome_da_função():
    print("Olá amigo, vamos fazer cálculos com funções do python?")

nome_da_função()

# TAREFA (21/10): utilizar o input
# input()
num_a = int(input("Insira um número: "))
num_b = int(input("Insira um número : "))
num_c = int(input("Insira um número : "))

#print(subtrair, type(subtrair)) # TAREFA: fazer print em f-string
# SUBTRAÇÃO
def subtrair():
    subtracao = num_a - num_b - num_c
    print(f"Início da subtração...\nA subtração {num_a} - {num_b} - {num_c} é igual a: {subtracao}; este resultado é um tipo de dado basico: {type(subtracao)}\nFim da subtração.")
    return subtracao

#SOMA
def somar():
    soma = num_a + num_b + num_c
    print(f"Início da soma...\nA soma de {num_a} + {num_b} + {num_c} é igual a: {soma}; este resultado é um tipo de dado basico: {type(soma)}\nFim da soma.")
    return soma

# MULTIPLICAÇÃO
def multiplicar():
    multiplicacao = num_a * num_b * num_c
    print(f"Início da multiplicação...\nA multiplicação de {num_a} por {num_b} por {num_c} é igual a: {multiplicacao}; este resultado é um tipo de dado basico: {type(multiplicacao)}\nFim da multiplicação.")
    return multiplicacao

#DIVISÃO
def dividir():
    divisao = num_a / num_b / num_c
    print(f"Início da divisão...\nA divisão de {num_a} por {num_b} por {num_c} é igual a: {divisao}; este resultado é um tipo de dado basico: {type(divisao)}\nFim da divisão.")
    return divisao

