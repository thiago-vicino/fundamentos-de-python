#from aula02_funcoes import nome_da_função

def tipos_avancados():
    lista1 = [1, 'thiago', 'Brasil', 'thiago']
    tupla1 = (1, 'thiago', 'Brasil')
    conjunto1 = set(lista1) #série não ordenada. Não dá para percorrer os elementos (não é iterável)
    dicionario1 = {1:'thiago',2:'Ana', 3:'Rafael'}
    dicionario1.update({2:['Ana Júlia']})
    print(type(dicionario1))
    return lista1

def controle_fluxo():
    lista = tipos_avancados()
    for percorrer in lista:
        print(str(percorrer).upper())
        if percorrer == 'thiago':
            print(percorrer)
    lista2 = [str(item).upper() for item in lista]
    print(f'Lista2: {lista2}')

def main ():
    #tipos_avancados()
    #nome_da_função()
    controle_fluxo()

if __name__ == "__main__": 
    main ()
