# for percorre a string (iterador)

# dander: __
# nas f srings só é necessário o parênteses se tiver quebra de linha (barra invertida + n)

# classes são conjuntos de funções

class CalculadoraSimples:
    def __init__(self, num_x, num_y):
        self.num_x = num_x 
        self.num_y = num_y
        
        #esses são os atributos

    def somar(self): 
        ''' Responsável por realizar a soma '''
        return f'A soma de {self.num_x} + {self.num_y} é igual a: {self.num_x + self.num_y}'
        
    def subtrair(self):
        return f'A subtração de {self.num_x} - {self.num_y} é igual a: {self.num_x - self.num_y}'
 
    def multiplicar(self):
        return f'A multiplicação de {self.num_x} por {self.num_y} é igual a: {self.num_y * self.num_x}'

    def dividir(self):
        return f'A divisão de {self.num_x} por {self.num_y} é igual a: {self.num_x / self.num_y}'

class Pizza:
    def __init__(self, qtd_pedacos, sabor):
        self.qtd_pedacos = qtd_pedacos
        self.sabor = sabor

    def valor(self):
        return f'Valor: R$ {self.qtd_pedacos * 8} !!!!!'

    def portuguesa(self):
        preco = 12
        return (f'Ingredientes: Molho de tomate, mussarela, presunto, palmito, ervilha, lombo canadense fatiado, provolone ralado e ovo. \n'
        f'O preço final é: {preco*self.qtd_pedacos}')

    def margherita(self):
        return f'Ingredientes: Molho de tomate, mussarela, manjericão e parmesão.'

def main ():
    '''print('Realizar operações simples: soma, subtração, multiplicação e divisão')
    num_x = float(input("Insira um número: "))
    num_y = float(input("Insira outro número: "))
    valores = CalculadoraSimples(num_x, num_y)
    print(valores.somar())
    print(valores.subtrair())
    print(valores.multiplicar())
    print(valores.dividir())'''
    print('####### BEM-VIND@ À PIZZARIA SHOW DE BOLA HMMMM!')
    qtd_pedacos = int(input('Por favor, insira o número de pedaços da pizza, 4, 6 ou 8: '))
    sabor = str(input('Por favor, insira o sabor da pizza, margherita ou portuguesa: '))
    preco = Pizza(qtd_pedacos, sabor)
    if sabor == 'portuguesa':
        sabor = 'portuguesa'
        print(preco.portuguesa())
    elif sabor == 'margherita':
        sabor = 'margherita'
        print(preco.margherita())
    # print(preco.valor())
    print('Achou caro? É culpa da política econômica de Guedes e Bolsonaro!!!')
    
if __name__ == "__main__": 
    main ()

''' TAREFA:  
1. Incluir multiplicar e dividir (ok)
2. Utilizar o input para os números (ok)
3. Criar uma classe chamada "pizza". (ok)
    a. incluir como atributos a quantidade de pedaços que você quer e o sabor;
    b. os métodos serão mussarela e calabresa ou qualquer outro sabor
    c. retornar os ingredientes e o valor da pizza
'''