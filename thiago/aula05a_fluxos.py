
def loop_for():
    pizzas = ['1-muçarela', '2-calabresa', '3-frango', '4-portuguesa'] #lista (elemento iterável)
    bebidas = ['g-guaraná', 'c-coca-cola', 'l-suco de laranja', 'u-suco de uva']
    brindes = ['chaveiro', 'caneta', 'balinha', 'abridor de lata']
    #print(type(pizzas))
    for pizza in pizzas: #'pizza' é o elemento iterador (às vezes aparece como 'i')
        print(pizza) #printa o elemento iterador
    for combo in zip(pizzas, bebidas, brindes):
        print(combo, type(combo))
        print(list(combo), type(list(combo)))
    for index in enumerate(pizzas, start=10):
        print(index)
    for num in range(10,20,2):
        print(num)

def loop_while():
    pizzas = ['muçarela', 'calabresa', 'frango', 'portuguesa'] 
    contador = 0
    while contador <= 10: # verifica se a expressão é verdadeira
#        print(pizzas) #vai printar enquanto for verdadeiro. Para parar, usar Ctrl+C
        print(f'O contador é: {contador}')
        contador = contador + 1 #controle para não repetir ad infinitum
    contador = 10
    lista_paginas = []

    while contador < 100: #limitador
        dominio = 'https://www.gov.br/pt-br/noticias/ultimas-noticias?b_start:int='
        dominio = dominio + str(contador)
        contador = contador + 10
        print(dominio)
        lista_paginas.append(dominio) # função append dentro de uma classe: método de lista

    print(lista_paginas)


def main ():
    loop_for()
#    loop_while()

if __name__ == "__main__": 
    main ()

