from functools import reduce


def func_lambda(x, y):
    conta = lambda x,y: x+y
    return f'A soma é: {conta(x,y)}'

def func_map():
    itens = [10, 20, 30, 40, 50]
    pizzaria = ['muçarela', 'guaraná']
    exemplo = list(map(str, itens)) 
    return exemplo

def dobrar(x):
    return x*2

def func_map2():
    itens = [1, 2, 3, 4, 5]
    itens2 = ['bode', 'vaca', 'porco']
    exemplo6 = list(map(str.title, itens2))
    return exemplo6

def func_map_similar():
    itens = [10, 20, 30, 40, 50]
    lista_itens = []
    for item in itens:
        lista_itens.append(str(item))
    return lista_itens

def func_reduce(): #precisa importar, vide linha 1
    '''
    - reduce (função, lista_elementos)
    '''
    itens = [10, 20, 30, 40, 50]
    exemplo = reduce(lambda x,y:x+y, itens)
    exemplo02 = reduce((lambda x,y: x if(x<y) else y), itens)
    return exemplo02

def somar(sabor,bebida):
    return sabor*bebida

def func_reduce2():
    pedido = [15, 3, 10]
    preco_final = reduce(somar, pedido)
    return preco_final

def oferta_do_dia(x,y):
    if x<y:
        return x
    else:
        return y

def func_reduce3():
    valores_sabor = [15, 30, 45]
    oferta_dia = reduce(oferta_do_dia, valores_sabor)
    return oferta_dia

def func_filter():
    '''
    - filter (função, lista_elementos)
    '''
    itens = [11, 20, 30, 40, 50]
    exemplo = list(filter(lambda x: x>=40, itens))
    return exemplo

def valor_a_partir_de(x):
    return x<=30

def func_filter2():
    itens = [5, 15, 30, 45, 60]
    ex = list(filter(valor_a_partir_de, itens))
    ex2 = list(filter(lambda x: x<=30, itens))
    return (f'Exemplo com função {ex}\n'
    f'Exemplo com lambda {ex2}')

def main ():
    '''
    while False:
        x = int(input('Insira um número inteiro: '))
        y = int(input('Insira outro número: ')) 
        exemplo01 = func_lambda(x, y)
        print(exemplo01)

    exemplo05 = func_filter()
    print(exemplo05)
    
    exemplo02 = func_map()
    print(exemplo02)
    print(func_map2())
    exemplo03 = func_map_similar()
    print(exemplo03)'''
    exemplo04 = func_reduce()
    print(exemplo04)
    print(func_reduce2())
    print(func_reduce3())
    print(func_filter2())
if __name__ == "__main__": 
    main ()

