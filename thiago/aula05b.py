def return_break_continue():
    pizzas = ['muçarela', 'calabresa', 'frango', 'portuguesa'] #lista (elemento iterável)
    bebidas = ['guaraná', 'coca-cola', 'suco de laranja', 'suco de uva']
    brindes = ['chaveiro', 'caneta', 'balinha', 'abridor de lata']
    for pizza in pizzas:
        if pizza == 'muçarela': #primeira condicional
            print(f'O valor da pizza de {pizza} é: R$10,00')
            continue
        elif pizza == 'frango':
        #    print (f'O valor da pizza de {pizza} é: R$11,00')
            return f'O valor da pizza de {pizza} é: R$11,00'
        else:
            print(f'O valor da pizza de {pizza} é: R$12,00')

        if pizza == 'muçarela': #segunda condicional
            print(f'O valor da pizza de {pizza} é: R$20,00')
    return pizzas

def try_except():
    pizzas = ['muçarela', 'calabresa', 'frango', 'portuguesa'] 
    try: 
        print(pizzas[4])
    except IndexError as erro:
        print(erro)
    print(pizzas[0])

def main ():
    exemplo1 = return_break_continue()
    print(exemplo1)
    try_except()

if __name__ == "__main__": 
    main ()

'''
Integrar elementos
'''