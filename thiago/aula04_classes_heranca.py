
class Pizza:
    def __init__(self, qtd_pedacos, sabor, formato='redonda'): #padrão (pode ser substituído se for passado novo formato)
        self.qtd_pedacos = qtd_pedacos
        self.sabor = sabor
        self.formato = formato
        

    # métodos:
    def ingredientes_geral (self):
        ingredientes = 'molho de tomate, farinha de trigo, queijo...'
        return ingredientes

    def preco (self):
        valor = self.qtd_pedacos*5
        return valor

    def tempo_de_espera (self):
        tempo = '30 minutos'
        return tempo

    def formato_pizza (self):
        if self.formato == '':
            return f'O formato da pizza é: redondo'
        else: 
            return f'O formato da pizza é: {self.formato}'

class Portuguesa(Pizza): 
    def __init__(self, qtd_pedacos, sabor, formato='redonda'):
        super().__init__(qtd_pedacos, sabor, formato)
        self.brinde = 'Guaraná'
    
    def ingredientes_geral(self):
        ingredientes = 'Ingredientes da pizza portuguesa: molho de tomate, mussarela, presunto, palmito, ervilha, lombo canadense fatiado, provolone ralado e ovo.'
        return ingredientes

    def recompensar (self):
        recompensa = f'Você ganhou dois {self.brinde}'
        return recompensa

    def tempo_de_espera (self):
        tempo = '50 minutos'
        return tempo

class Margherita(Pizza):
    def __init__(self, qtd_pedacos, sabor, formato='redonda'):
        super().__init__(qtd_pedacos, sabor, formato)
        self.cupom = 0.5

    def preco (self):
        valor = self.qtd_pedacos*5*self.cupom
        return valor

class Vegetariana(Pizza):
    def __init__(self, qtd_pedacos, sabor, formato='redonda'):
        super().__init__(qtd_pedacos, sabor, formato)
        self.cupom = 0 #cupom fixo

    def preco (self):
        valor = self.qtd_pedacos*5*self.cupom
        return valor

class Bacon(Pizza):
    def __init__(self, qtd_pedacos, sabor, formato='redonda'):
        super().__init__(qtd_pedacos, sabor, formato)

    def preco (self):
        valor = self.qtd_pedacos*5
        return valor


def main ():
    sabor = input('Escolha entre margherita ou portuguesa: ')
    qtd_pedacos = int(input('Por favor, insira o número de pedaços da pizza, 4, 6 ou 8: '))
    formato = str(input('Por favor, insira o formato da pizza: redonda ou quadrada: '))
    if sabor == 'margherita':
        pedido = Margherita(qtd_pedacos, sabor, formato)
        ingredientes = pedido.ingredientes_geral()
        print(ingredientes)
        valor = pedido.preco()
        print(valor)
    elif sabor == 'portuguesa':
        pedido = Portuguesa(qtd_pedacos, sabor, formato)
        Portuguesa_ingredientes = pedido.ingredientes_geral()
        print(Portuguesa_ingredientes)
        valor = pedido.preco()
        print(valor)
        recompensa = pedido.recompensar()
        print(recompensa)
        forma = pedido.formato_pizza()
        print(forma)
        tempo = pedido.tempo_de_espera()
        print(tempo)
if __name__ == "__main__": 
    main ()

