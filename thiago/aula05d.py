# Compreensão de listas:

# [expressão for item in lista_itens]
def listas01():
    sabores = ['muçarela', 'calabresa', 'portuguesa']
    lista_itens = []
    for sabor in sabores:
        print(sabor.upper())
        lista_itens.append(sabor.upper())
    return lista_itens

def listas02():
    sabores = ['muçarela', 'calabresa', 'portuguesa']
    lista_itens = [item.upper() for item in sabores]
    return (f'Listas02: {lista_itens}')

def listas03():
    sabores = ['muçarela', 'calabresa', 'portuguesa']
    #lista_itens = [sabor.title() for sabor in sabores if sabor != 'calabresa' else "calabresa"] # " != " é o símbolo para "diferente de"
    lista_itens = ["calabresa" if sabor != "calabresa" else "sem sabor" for sabor in sabores]
    return (f'Listas03: {lista_itens}')

def main ():
    print(listas01())
    print(listas02())
    print(listas03())

if __name__ == "__main__": 
    main ()