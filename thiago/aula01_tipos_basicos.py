# Comentário

"""
DocsStrings

"""


#################
# Variáveis
#################
"""
- Variável é uma etiqueta colocada em determinado valor. Quando precisamos desse valor, "chamamos" através da etiqueta
- operador de atribuição =
- regras de nomeação de variáveis
  - Não podem ter espaço e devem começar com letra ou sublinhado
  - Não podem usar palavras reservadas ou identificadores internos que tenham finalidades importantes em Python
  - Por convenção, pede-se que se utilize apenas letras minúsculas e sublinhado para nomear as variáveis

"""

cumprimento = "Olá Mundo!" # Tipo de dado Basico - String
cumprimento_boas_vindas = "Vejam Bem Vindos!"

print(cumprimento)


"""
# tipos de dados basicos
 - string (str) 
 >> Uma string pode ser definida como uma lista de caracteres. 
 É uma série ordenada e imutável de caracteres (números, letras, símbolos, espaços e pontuações) 
>> Para se definir uma string, ela deve ser colocada entre aspas simples 'string'
>> indices e manipulação de strings
 - [inicio:fim:salto]

>> f-strinf
- numeros (int, float, complex)

"""

# operações com strings

data = "21/10/2021"
#indice 012345...
nome = "Universidade Estadual Paulista - UNESP" # ITERAVEL
salto = "AA_AA_AA_AA"

# ITERAVEL E ITERADOR (caracter)
for caracter in nome[-5:]:
    print(caracter)
print("Terminou o loop for - fluxo de repetição ")

print(salto[::3])

# + ou *

print("Thiago e " + "Ana na " + nome )
print("Thiago "* 2 + "Ana "*2 )

unesp = f'Thiago e Ana na {nome}' # f-string
print(unesp)

# numeros - tipo de dado básico

# num_x = float("3") # 3.0
# num_y = 9.5
# num_w = 12

# input()
num_x = int(input("Insira um numero inteiro: "))
num_y = int(input("Insira um numero inteiro: "))
num_w = int(input("Insira um numero inteiro: "))

#print(subtrair, type(subtrair)) # TAREFA: fazer print em f-string
# SUBTRAÇÃO
subtrair = num_x - num_y - num_w
print("##### Inicio da subtração...")
print(f'A subtração {num_x} - {num_y} - {num_w} é igual a: {subtrair}; este resultado é um tipo de dado basico: {type(subtrair)}' )
print("##### Fim da subtração...")

#SOMA
somar = num_x + num_y + num_w
print("##### Inicio da soma...")
print(somar, type(somar))  # TAREFA: (21/10)fazer print em f-string
print("##### Fim da soma...")

# MULTIPLICAÇÃO
multiplicar = num_x * num_y * num_w
print("###### Inicio da multiplicação...")
print(multiplicar, type(multiplicar))  # TAREFA(21/10): fazer print em f-string
print("###### Fim da multiplicação...")
