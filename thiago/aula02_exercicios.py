from aula03_classes import inverter


#print('TAREFA (21/10): montar função de multiplicação e subtração')
#comprenssão de lista: a, b, c = [int(x) for in input('Digite todos os valores: ).split()]

def numeros():
    num_a, num_b, num_c = [int(x) for x in input('Digite todos os valores:').split()]
    '''
    num_a = int(input('Insira um número inteiro: '))
    num_b = int(input('Insira outro número inteiro: '))
    num_c = int(input('Insira mais um número inteiro: '))
    '''
    return num_a, num_b, num_c

def somar(num_a , num_b , num_c): 
    ''' Responsável por realizar a soma '''
    soma = f'A soma de {num_a} + {num_b} + {num_c} é igual a: {num_a + num_b + num_c}'
    print(soma)
    return soma

def subtrair(num_a, num_b, num_c):
    subtracao = f'A subtração de {num_a} - {num_b} - {num_c} é igual a: {num_a - num_b - num_c}'
    print(subtracao)
    return subtracao

def multiplicar(num_a, num_b, num_c):
    multiplicacao = f'A multiplicação de {num_a} por {num_b} por {num_c} é igual a: {num_a * num_b * num_c}'
    print(multiplicacao)
    inversao = inverter(num_a, num_b, num_c)
    return multiplicacao

def dividir(num_a, num_b, num_c):
    divisao = f'A divisão de {num_a} por {num_b} por {num_c} é igual a: {num_a / num_b / num_c}'
    print(divisao)
    return divisao


'''
def numeros():
    numero_01 = int(input('Insira um número: '))
    numero_02 = int(input('Insira outro número: '))
    return numero_01 , numero_02

def somar():
    numeros_todos = numeros()
    print (numeros_todos)
    somar = numeros_todos [0] + numeros_todos [1]
    print (f'A soma de {numeros_todos [0]} com {numeros_todos [1]} é: {somar}')
    return somar

def subtrair():
    soma = somar()
    print (soma)
    numero = int(input (f'Indique o número para subtrair de {soma}: '))
    subtraçao = soma - numero
    print (f'O resultado da subtração é: {subtraçao}')
    #return print (subtraçao)'''

def main ():
    num_a , num_b , num_c = numeros()
    somar_numeros = somar(num_a , num_b , num_c)
    subtrair_numeros = subtrair(num_a , num_b , num_c)
    dividir_numeros = dividir(num_a , num_b , num_c)
    multiplicar_numeros = multiplicar(num_a , num_b , num_c)


if __name__ == "__main__": 
    main ()

